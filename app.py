from flask import Flask, render_template, request, redirect, url_for, flash

app = Flask(__name__)
app.secret_key = 'many random bytes'

@app.route('/hola')
def index():
    return "hola mundo"


@app.route('/', methods=['POST', 'GET'])
def hola():
    #flash("Hola")
    data=[1,2,3,4]
    context = {
        'data': data,
    }
    return render_template('index.html', **context)

if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0",port=8080)
